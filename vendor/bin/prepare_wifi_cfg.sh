#!/vendor/bin/sh

SOC_ID=`cat /sys/devices/soc0/soc_id`

# soc_ids SM8450: 457, 482
if [ "${SOC_ID}" == "457" -o "${SOC_ID}" == "482" ]; then
    SRC_DIR="/vendor/etc/wifi/qca6490"
else
    SRC_DIR="/vendor/etc/wifi"
fi

VERSION_STR=`grep version ${SRC_DIR}/version.txt`
setprop ro.vendor.wifi.cfg_version ${VERSION_STR:8}
